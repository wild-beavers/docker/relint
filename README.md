# Relint

## Description
This image contains relint: https://github.com/codingjoe/relint

The image is based on the official alpine image.

## Tags
We push a `latest` tag on this repository, to run an older version please checkout the different tags.

## Usage

## Labels
We set labels on our images with additional information on the image.
We follow the guidelines defined at http://label-schema.org/.
Visit their website for more information about those labels.

## Contributions
Contributions are welcome.
