ARG IMAGE="docker.io/library/alpine"
ARG IMAGE_TAG="3.18"

FROM $IMAGE:$IMAGE_TAG

ENV PYTHON3_VERSION=3.11.6-r0
ENV RELINT_VERSION=3.1.0

ARG IMAGE
ARG IMAGE_TAG
ARG BUILD_DATE
ARG VCS_REF
ARG VCS_DESCRIPTION
ARG VCS_URL
ARG VCS_TITLE
ARG VCS_NAMESPACE
ARG LICENSE
ARG VERSION

VOLUME /data

COPY resources /resources

RUN /resources/build && rm -rf /resources

WORKDIR /data

ENTRYPOINT ["relint"]

LABEL "org.opencontainers.image.created"=${BUILD_DATE} \
      "org.opencontainers.image.authors"="${VCS_NAMESPACE} - https://gitlab.com/wild-beavers/" \
      "org.opencontainers.image.url"="${VCS_URL}" \
      "org.opencontainers.image.documentation"="${VCS_URL}" \
      "org.opencontainers.image.source"="https://github.com/codingjoe/relint" \
      "org.opencontainers.image.version"="${VERSION}" \
      "org.opencontainers.image.vendor"="${VCS_NAMESPACE}" \
      "org.opencontainers.image.licenses"="${LICENSE}" \
      "org.opencontainers.image.title"="${VCS_TITLE}" \
      "org.opencontainers.image.description"="${VCS_DESCRIPTION}" \
      "org.opencontainers.image.base.name"="${IMAGE}:${IMAGE_TAG}" \

      "org.opencontainers.applications.relint.version"="$RELINT_VERSION" \
      "org.opencontainers.dependencies.python3"="$PYTHON3_VERSION"
