# 3.1.0-2

- fix: change entrypoint to `relint` instead of `aws`

# 3.1.0-1

- tech: aligns with relint versioning

# 0.0.0

- tech: initial version
